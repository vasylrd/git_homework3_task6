import math


def show_header() -> None:
    print(
        "Welcome to the program for calculating square and cube roots.",
        "Please enter one of the following options:",
        "'sqrt' - square root of x.",
        "'cbrt' - root of x.",
        "'sine' - sine of x.",
        "'cosine' - cosine of x."
        "",
        sep="\r\n",
    )


def get_result(cmd: str, val: float) -> float:
    cmd = cmd.strip().lower()
    if cmd == "sqrt":
        return math.sqrt(val)
    elif cmd == "cbrt":
        return math.cbrt(val)
    elif cmd == "sine":
        return math.sin(val)
    elif cmd == "cosine":
        return math.cos(val)
    else:
        raise ValueError(f"Option {cmd!r} not found!")


def main() -> None:
    show_header()
    while True:
        input_raw = input(
            "Enter the option and value, separated by a space ('q' for exit): "
        )
        if input_raw.strip().lower() == "q":
            break

        try:
            option, value_raw = input_raw.strip().lower().split()
        except ValueError:
            print("Enter two items: option and value!")
            continue
        try:
            value = float(value_raw)
            result = get_result(option, value)
        except ValueError as exp:
            print(exp)
            continue
        print(f"{option}({value}) = {result}")


if __name__ == "__main__":
    main()
